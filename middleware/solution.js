import { firestore } from '~/plugins/firebase'

const collection = 'solutions'

export const getSolutions = () => {
  return firestore.collection(collection).orderBy('date', 'desc').get()
}
